# 1.2.8
Fixed for new version of Lethal Company. Please if you can drop a 1$ donation I lost my job and would love some motivation.

# 1.2.7
Probably Fixed Sync Error With Turret And Enemy Deaths And Damage.

# 1.2.6
Probably fixed the soft dependency issue for boombas and also the turrets now no longer target enemies but will damage them. Will make them target enemies in a later update. Turrets also are full auto again

# 1.2.5 
Probably fixed turret lag

# 1.2.4 
Added more config options

# 1.2.3 
Put in a new Turret AI to kill enemies. Let me know how I did!

# 1.2.2 
Took out my test ai for turrets. Sorry for the problems

# 1.2.1 
Added a check to make sure mob and item names are valid

# 1.2.0 
Big update: After adding support for the boomba from LethalThings you can now config what can set them off.

# 1.0.15 
Added support for the moving landmine boomba to do the same as the previous update.

# 1.0.14 
Added Mines that can be set off anything considered an enemy and as long as they are killable they can be killed.