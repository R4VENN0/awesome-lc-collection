# Land Mine Fart with Reverb
Replaces the detonation sounds with a fart with reverb.

Compatible with V47 and all other mods. This is a client-side mod and only those with it installed will have its effects.

## r2modman Installation
1. Via r2modman, install BepInEx
2. Via r2modman, install LandMineFartReverb

## Thunderstore App Installation
- NOT TESTED. Install at own risk

## Manual Installation
1. Install BepInEx
2. Extract LandMineFartReverb.dll and fartiwhtreverb into the plugins folder of BepInEx
3. Light a match, will ya

## Preview
See the linked video for a preview of the mod: https://youtu.be/sDMMn43_GWo

## Changes
- 1.0.1: Added r2modman support. This changes the file structure dependency. The asset pack now sits in the plugin folder.
- 1.0.2: Updated readme.
- 1.0.3: Updated readme.